# fizzbuzz

In this project I wil make the simple Fizz Buzz Interview question with a few additions.

## features

In this version however I'll make it as flexible as possible:

* use a `config.json` file
* adding custom triggers
* adding custom handlers
* adding custom types (generified)

## languages

also I'l make it in all languages I know well enough

* java (8)
* python (3.6)
* javasciprt (ES6)
* php (7)