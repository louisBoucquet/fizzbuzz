import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class FizzBuzzerTest {
    private FizzBuzzer<Integer, String> fizzBuzzer;

    @Before
    public void setUp() throws Exception {
        Map<Integer, String> triggers = new HashMap<>();
        triggers.put(3, "Fizz");
        triggers.put(5, "Buzz");
        triggers.put(7, "Shit");

//        BiFunction<Integer, Set<String>, Object> bla = (i, set) -> set.size() == 0 ? i : set;
//        bla.

        fizzBuzzer = new FizzBuzzer<>(
                triggers,
                (key, i) -> i % key == 0,
                (i, set) -> {
                    System.out.print(i);
                    System.out.print(' ');
                    System.out.println(set);
                });
    }

    @Test
    public void buzz() {
        fizzBuzzer.buzz(1);
        fizzBuzzer.buzz(3);
        fizzBuzzer.buzz(5);
        fizzBuzzer.buzz(15);
    }

    @Test
    public void buzzRange() {
        fizzBuzzer.buzzRange(Stream
                .iterate(0, i -> ++i)
                .limit(3 * 5 * 7 + 1)
                ::iterator);
    }
}